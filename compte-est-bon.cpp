#include <cstdlib>
#include <vector>
#include <random>
#include <iostream>

/**
 * Enumération des opérations
 */
enum Operations
{
    addition,
    soustraction,
    produit,
    division
};
// Tableau des opérations
static const char *OperationsStrings[] = {"+", "-", "*", "/"};

/**
 * Classe de nombre
 */
class Nbr
{
public:
    int valeur;
    std::string str;
    Nbr(int valeur)
    {
        this->valeur = valeur;
        this->str = std::to_string(valeur);
    }
    Nbr()
    {
        this->valeur = -1;
        this->str = std::to_string(valeur);
    }
    std::string toString()
    {
        return str;
    }
};

/**
 * Classe de nombre calculé
 */
class NbrCalcule : public Nbr
{
public:
    Nbr *nbr1;
    Nbr *nbr2;
    Operations operation;
    NbrCalcule(Nbr &nbr1, Nbr &nbr2, Operations operation)
    {
        this->nbr1 = &nbr1;
        this->nbr2 = &nbr2;
        this->operation = operation;
        this->str = "(" + nbr1.toString() + " " + OperationsStrings[operation] + " " + nbr2.toString() + ")";
        switch (operation)
        {
        case addition:
            valeur = nbr1.valeur + nbr2.valeur;
            break;
        case soustraction:
            valeur = nbr1.valeur - nbr2.valeur;
            break;
        case produit:
            valeur = nbr1.valeur * nbr2.valeur;
            break;
        case division:
            if (nbr2.valeur == 0 || nbr1.valeur % nbr2.valeur != 0)
            {
                throw std::invalid_argument(std::string(OperationsStrings[operation]) + " impossible avec " + nbr1.toString() + " par " + nbr2.toString());
            }
            valeur = nbr1.valeur / nbr2.valeur;
            break;
        }
    }
};

// Paramètres
const std::vector<Nbr> nbrPossibles = {1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 25, 50, 75, 100};
const int nbNbr = 6;

// Variables globales pour la résolution
Nbr best = Nbr(99999);
int but;

/**
 * Fonction de sauvegarde de la meilleure solution
 * @param solution La solution à sauvegarder si meilleure
 */
void saveBest(NbrCalcule solution)
{
    if (abs(solution.valeur - but) < abs(best.valeur - but))
    {
        best = solution;
    }
}

/**
 * Fonction de résolution du jeu "le compte est bon"
 * @param tirage Le tirage de nombres
 */
void resoudre(std::vector<Nbr> tirage)
{
    if (best.valeur == but)
    {
        return;
    }
    if (tirage.size() < 2)
    {
        return;
    }
    for (int i = 0; i < tirage.size(); i++)
    {
        for (int j = 0; j < tirage.size(); j++)
        {
            if (i != j)
            {
                for (int k = 0; k < 4; k++)
                {
                    Operations operation = (Operations)k;
                    if ((operation == addition || operation == produit) && i > j)
                    {
                        continue;
                    }
                    try
                    {
                        NbrCalcule nbrCalcule = NbrCalcule(tirage[i], tirage[j], operation);
                        std::vector<Nbr> tirage2 = tirage;

                        if (i > j)
                        {
                            tirage2.erase(tirage2.begin() + i);
                            tirage2.erase(tirage2.begin() + j);
                        }
                        else
                        {
                            tirage2.erase(tirage2.begin() + j);
                            tirage2.erase(tirage2.begin() + i);
                        }

                        tirage2.push_back(nbrCalcule);
                        saveBest(nbrCalcule);
                        resoudre(tirage2);
                    }
                    catch (std::invalid_argument &e)
                    {
                        // affichage de l'erreur
                        // std::cout << e.what() << "\n";
                    }
                }
            }
        }
    }
}

/**
 * Fonction principale du programme
 */
int main()
{
    // Initialisation du générateur de nombres aléatoires
    std::mt19937 mt(755486815753);
    std::vector<Nbr> tirage;

    // Initialisation du tirage
    but = mt() % 900 + 100;
    std::vector<Nbr> nbrPossibles2 = nbrPossibles;
    for (int i = 0; i < 6; i++)
    {
        int r = mt() % nbrPossibles2.size();
        tirage.push_back(nbrPossibles2[r]);
        std::cout << nbrPossibles2[r].toString() << " ";
        nbrPossibles2.erase(nbrPossibles2.begin() + r);
    }

    std::cout << "\n"
              << but << "\n\n";

    // Résolution du jeu
    resoudre(tirage);

    // Affichage de la meilleure solution
    std::cout << "Meilleure solution : " << best.toString() << " = " << best.valeur << "\n";

    return 0;
}